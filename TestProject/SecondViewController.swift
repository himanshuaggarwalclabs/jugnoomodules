//
//  SecondViewController.swift
//  TestProject
//
//  Created by Samar Singla on 04/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, 375, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        
        let label1 = UILabel()
        label1.text = "Second"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, 40, 150, 30)
        view1.addSubview(label1)

        
        let loginButton = UIButton()
        loginButton.setTitle("Back", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        loginButton.layer.cornerRadius = 5
        loginButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+5, 330, 60)
        loginButton.addTarget(self, action: "backPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginButton)
        
        let gotoButton = UIButton()
        gotoButton.setTitle("goto", forState: .Normal)
        gotoButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        gotoButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        gotoButton.layer.cornerRadius = 5
        gotoButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+100, 330, 60)
        gotoButton.addTarget(self, action: "gotoPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(gotoButton)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func backPressed(){
        //let testView = self.storyboard?.instantiateViewControllerWithIdentifier("firstView") as ViewController
        //self.navigationController?.popToViewController(testView, animated: true)
        // self.navigationController?.popToRootViewController(animated: true)
       self.navigationController?.popToRootViewControllerAnimated(true)
    }
    func gotoPressed(){
        let thirdView = self.storyboard?.instantiateViewControllerWithIdentifier("thirdView") as ThirdViewController
        //println(thirdView)
        self.navigationController?.pushViewController(thirdView, animated: true)

    }
}
