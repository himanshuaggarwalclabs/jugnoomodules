//
//  ThirdViewController.swift
//  TestProject
//
//  Created by Samar Singla on 04/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, 375, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        
        let label1 = UILabel()
        label1.text = "Third"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, 40, 150, 30)
        view1.addSubview(label1)

        let loginButton = UIButton()
        loginButton.setTitle("Back", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        loginButton.layer.cornerRadius = 5
        loginButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+5, 330, 60)
        loginButton.addTarget(self, action: "backPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginButton)
        
        let gotoButton = UIButton()
        gotoButton.setTitle("Whatsapp", forState: .Normal)
        gotoButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        gotoButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        gotoButton.layer.cornerRadius = 5
        gotoButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+100, 330, 60)
        gotoButton.addTarget(self, action: "gotoPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(gotoButton)


        // Do any additional setup after loading the view.
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)



    }
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent) {
        if event.subtype == UIEventSubtype.MotionShake {
            println("User Shook Thief Device")
        }
    }
    // SwipeGesture
    func swiped(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                println("User swiped Right")
                 navigationController!.popToViewController(navigationController!.viewControllers[1] as UIViewController, animated: true)
            case UISwipeGestureRecognizerDirection.Left:
                println("User swiped Left")
                
            case UISwipeGestureRecognizerDirection.Up:
                println("User swiped Up")
            case UISwipeGestureRecognizerDirection.Down:
                println("User swiped Down")
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func backPressed(){
        //let testView = self.storyboard?.instantiateViewControllerWithIdentifier("secondView") as SecondViewController
       // println(testView)
       // self.navigationController?.popToViewController(testView, animated: true)
        navigationController!.popToViewController(navigationController!.viewControllers[1] as UIViewController, animated: true)
    }*/
         //self.navigationController?.popToRootViewController(animated: true)
          //self.navigationController?.popToViewController(SecondViewController, animated: true)

    
        func gotoPressed(){
            //var whatsappURL:NSURL? = NSURL(string: "whatsapp://send?text=123Testing%2C%20World!")
            //if (UIApplication.sharedApplication().canOpenURL(whatsappURL!)) {
              //  UIApplication.sharedApplication().openURL(whatsappURL!)
            var url:NSURL = NSURL(string: "tel://9809088798")!
            UIApplication.sharedApplication().openURL(url)
            }
    
    

}
