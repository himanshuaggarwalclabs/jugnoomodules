//
//  ViewController.swift
//  TestProject
//
//  Created by Samar Singla on 03/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit
import Foundation

let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
 var ridesTableView: UITableView  =   UITableView()
    
    var futureTableView: UITableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingRidescreen()
        self.ridesTableView.alpha = 1
        self.ridesTableView.tag  = 1
        self.futureTableView.tag = 2
        self.futureTableView.alpha = 0
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func creatingRidescreen(){
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, 375, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        
        let label1 = UILabel()
        label1.text = "Rides"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, 40, 150, 30)
        view1.addSubview(label1)
       
        
        let ridesButton = UIButton()
        ridesButton.setTitle("Ride History", forState: .Normal)
        ridesButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        //ridesButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        //ridesButton.layer.cornerRadius = 5
        ridesButton.frame = CGRectMake(screenWidth/2-150,screenHeight/2-250, 120, 35)
        ridesButton.addTarget(self, action: "ridePressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(ridesButton)
        
        let futureButton = UIButton()
        futureButton.setTitle("Future Rides", forState: .Normal)
        futureButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        //ridesButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        //futureButton.layer.cornerRadius = 5
        futureButton.frame = CGRectMake(screenWidth/2+30,screenHeight/2-250, 120, 35)
        futureButton.addTarget(self, action: "futurePressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(futureButton)

        
       
        ridesTableView.frame         =   CGRectMake(0,screenHeight-550, screenWidth, screenHeight+50);
        ridesTableView.delegate      =   self
        ridesTableView.dataSource    =   self
        
        ridesTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(ridesTableView)
        
        
        futureTableView.frame         =   CGRectMake(0,screenHeight-550, screenWidth, screenHeight+50);
        futureTableView.delegate      =   self
        futureTableView.dataSource    =   self
        
        futureTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(futureTableView)

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
            return 2
        }
        if tableView == 2 {
        return 1
        }
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        
        
        var cellView = UIView(frame: CGRectMake(2, 2, screenWidth, 120))
        //cellView.backgroundColor = UIColor.redColor()
        cell.contentView.addSubview(cellView)
        // var couponImage = UIImage(named:"coupon_account.png" )
        //var cellImage = UIImageView(image: couponImage!)
        //var cellImage = UIImageView(frame: CGRectMake(30, 5, 302, 114))
       // cellImage.image = UIImage(named: "coupon_account.png")
        //cellImage.backgroundColor = UIColor.greenColor()
        //cellView.addSubview(cellImage)
        
        if tableView.tag == 1{
            
        let cellLabel1 = UILabel()
        cellLabel1.text = " From :"
        cellLabel1.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        cellLabel1.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
        //label3.textColor = UIColor.blackColor()
        //label3.textAlignment = .Center
        //cellLabel1.numberOfLines = 5
        cellLabel1.frame = CGRectMake(10, 20, 80, 30)
        cellView.addSubview(cellLabel1)
        
        let cellLabel2 = UILabel()
        cellLabel2.text = " To :"
        cellLabel2.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        cellLabel2.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
        //label3.textColor = UIColor.blackColor()
        //label3.textAlignment = .Center
        //cellLabel2.numberOfLines = 5
        cellLabel2.frame = CGRectMake(10, 80, 80, 30)
        cellView.addSubview(cellLabel2)
        
        return cell

        }
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height: CGFloat = 200
        return height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
    }

    
    func ridePressed(){
        ridesTableView.alpha = 1
        futureTableView.alpha = 0
           
    }
    func futurePressed(){
        ridesTableView.alpha = 0
        futureTableView.alpha = 1
        
        let secondView = self.storyboard?.instantiateViewControllerWithIdentifier("secondView") as SecondViewController
        self.navigationController?.pushViewController(secondView, animated: true)

    }


}

